<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUser;
use Sentinel;
class AuthController extends Controller
{
    public function getRegister(){
    	return view('auth.register');
    }

    public function postRegister(RegisterUser $request){
    	
		$user = Sentinel::registerAndActivate($request->all());
		$role = Sentinel::findRoleBySlug('Admin');
		$role->users()->attach($user);
		return redirect(route('login.view'))->with('success','Now, You can Login');
    }

    public function getLogin(){
    	return view('auth.login');
    }
}
