<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register','AuthController@getRegister')->name('register.view');
Route::post('/register','AuthController@postRegister')->name('register.post');
Route::get('/login','AuthController@getLogin')->name('login.view');
Route::post('/login','AuthController@postLogin')->name('login.post');