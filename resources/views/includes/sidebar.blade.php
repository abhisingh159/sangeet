<div class="catagory-sidebar">
	<div class="banner-2-img mb-60">
		<a href="#"><img src="assets/img/banner/005.jpg" alt=""></a>
	</div>
	<div class="banner-2-img mb-60">
		<div class="section-title mb-30">
			<h4>Popular Category </h4>
		</div>
		<a href="#"><img src="assets/img/banner/02.png" alt=""></a>
	</div>
	<div class="social-wrapper mb-60">
		<div class="section-title mb-30">
			<h4>Join With Us</h4>
		</div>
		<div class="social-icon">
			<a class="fb" href="#"><i class="fab fa-facebook-f"></i></a>
			<a class="twit" href="#"><i class="fab fa-twitter"></i></a>
			<a class="insta" href="#"><i class="fab fa-instagram"></i></a>
			<a class="pin" href="#"><i class="fab fa-pinterest-p"></i></a>
			<a class="google" href="#"><i class="fab fa-google-plus-g"></i></a>
			<a class="dribbble" href="#"><i class="fab fa-dribbble"></i></a>
			<a class="app" href="#"><i class="fab fa-app-store"></i></a>
			<a class="behance" href="#"><i class="fab fa-behance"></i></a>
		</div>
	</div>
	<div class="hero-sidebar-item mb-60">
		<div class="section-title mb-30">
			<h4>Recommended</h4>
		</div>
		<div class="hero-sidebar arrow-active owl-carousel">
			<div class="hero-post-item hero-post-item-2">
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t6.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-3" href="#">tech</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Big firm products top worst plastic litter list report</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t7.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1" href="#">sports</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Bahurupi natya sangstha and  angan belgharia stage</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t8.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-2" href="#">lifestryle</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">DDhaka international folk fest 2019 to open on november</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t9.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-4" href="#">technology</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Big firm products top worst plastic litter list report</a></h4>
					</div>
				</div>
			</div>
			<div class="hero-post-item hero-post-item-2">
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t6.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-3" href="#">tech</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Big firm products top worst plastic litter list report</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t7.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1" href="#">sports</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Bahurupi natya sangstha and  angan belgharia stage</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix mb-20">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t8.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-2" href="#">lifestryle</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">DDhaka international folk fest 2019 to open on november</a></h4>
					</div>
				</div>
				<div class="post-sm-list fix">
					<div class="post-sm-img f-left">
						<a href="#"><img src="assets/img/news/t9.jpg" alt=""></a>
					</div>
					<div class="post-2-content fix">
						<div class="post-content-meta">
							<span><a class="meta-1 meta-4" href="#">technology</a></span>
							<span><a class="meta-11" href="#"><i class="far fa-clock"></i> 25 Nov 2019</a></span>
						</div>
						<h4><a href="#">Big firm products top worst plastic litter list report</a></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="newsletter mb-60">
		<div class="section-title mb-30">
			<h4>Newsletters</h4>
		</div>
		<div class="newsletter-wrapper" style="background-image:url(assets/img/hero/news.jpg)">
			<div class="newsletter-title">
				<h4>Subscribe Out Newsletters</h4>
				<h3>to get more news &amp; update</h3>
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</p>
			</div>
			<form id="newsletter-form" action="#">
				<input type="text" placeholder="Enter your name ">
				<input type="email" placeholder="Enter your email">
				<div class="contacts-us-form-button text-center">
					<div class="newsletter-button">
						<button>Subscribe Now</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="popular-tag-sidebar">
		<div class="section-title mb-30">
			<h4>Popular Tags</h4>
		</div>
		<div class="popular-tag">
			<a href="#">Newspaper</a>
			<a href="#">Magazine</a>
			<a href="#">Papers</a>
			<a href="#">Fashion</a>
			<a href="#">Lifestyle</a>
			<a href="#">Gym</a>
			<a href="#">News &amp; Blog</a>
			<a href="#">Sports</a>
			<a href="#">Music</a>
			<a href="#">Shopping</a>
			<a href="#">Swmming</a>
			<a href="#">Organic</a>
			<a href="#">Techonology</a>
			<a href="#">Travel</a>
			<a href="#">Lifestyle</a>
			<a href="#">Gaming</a>
		</div>
		<div class="banner-2-img mt-30">
			<a href="#"><img src="assets/img/banner/002.jpg" alt=""></a>
		</div>
	</div>

</div>