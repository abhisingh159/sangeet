<!-- footer-area-start -->
<footer>
	<div class="footer-area blue-bg pt-165">
		<div class="container">
			<div class="row">
				<div class="col-xl-3 col-lg-6 col-md-6">
					<div class="footer-wrapper mb-30">
						<h3 class="footer-title">About Us</h3>
						<div class="footer-text">
							<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system and expound the actual teachings of the great explorer of the truth the master-builder.</p>
						</div>
						<div class="social-icon footer-icon">
							<a class="fb" href="#"><i class="fab fa-facebook-f"></i></a>
							<a class="twit" href="#"><i class="fab fa-twitter"></i></a>
							<a class="insta" href="#"><i class="fab fa-instagram"></i></a>
							<a class="pin" href="#"><i class="fab fa-pinterest-p"></i></a>
							<a class="google" href="#"><i class="fab fa-google-plus-g"></i></a>
							<a class="dribbble" href="#"><i class="fab fa-dribbble"></i></a>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-md-6">
					<div class="footer-wrapper pl-30 mb-30">
						<h3 class="footer-title">Information</h3>
						<div class="footer-link">
							<ul>
								<li><a href="#">About Company</a></li>
								<li><a href="#">Latest News</a></li>
								<li><a href="#">Recent News</a></li>
								<li><a href="#">Meet With Us</a></li>
								<li><a href="#">Needs A Job ?</a></li>
								<li><a href="#">What We Do ?</a></li>
								<li><a href="#">Report Our News ?</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-md-3">
					<div class="footer-wrapper pl-25 mb-30">
						<h3 class="footer-title">About Us</h3>
						<div class="footer-link">
							<ul>
								<li><a href="#">Who We Are ?</a></li>
								<li><a href="#">Conatct Us</a></li>
								<li><a href="#">Advertisement Us</a></li>
								<li><a href="#">Join With Us</a></li>
								<li><a href="#">Setting & Privacy</a></li>
								<li><a href="#">Latest Blog</a></li>
								<li><a href="#">Entertainment</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-2 col-lg-4 col-md-4">
					<div class="footer-wrapper mb-30">
						<h3 class="footer-title">Contact Us</h3>
						<ul class="footer-info">
							<li><span><i class="far fa-map-marker-alt"></i> 1058 Meadowb, Mall Road</span></li>
							<li><span><i class="far fa-envelope-open"></i> <a href="http://bdevs.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="6a191f1a1a05181e2a0d070b030644090507">[email&#160;protected]</a></span></li>
							<li><span><i class="far fa-phone"></i> +000 (123) 44 558</span></li>
							<li><span><i class="far fa-paper-plane"></i> www.buildmartinfo.net</span></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-lg-6 col-md-5">
					<div class="footer-wrapper mb-30 pl-60">
						<h3 class="footer-title">Twitter Feeds</h3>
						<ul class="footer-info-link">
							<li>
								<div class="footer-info-icon f-left mr-20 ">
									<i class="fab fa-twitter"></i>
								</div>
								<div class="footer-content">
									<p><a href="#">"<span class="__cf_email__" data-cfemail="afe1cad8dcef">[email&#160;protected]</span></a> accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium "</p>
								</div>
							</li>
							<li>
								<div class="footer-info-icon f-left mr-20 ">
									<i class="fab fa-twitter"></i>
								</div>
								<div class="footer-content">
									<p><a href="#">"<span class="__cf_email__" data-cfemail="3a080e745f4d497a">[email&#160;protected]</span></a> On the other hand, we denounce with rindi natioand dislike men who are so beguiled and demoralized "</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-bottom-area mt-45 pt-25 pb-25 footer-border-top">
				<div class="row">
					<div class="col-xl-8 col-lg-12 offset-xl-2">
						<div class="footer-bottom-link text-center">
							<a href="#">About Us</a>
							<a href="#">Latest News</a>
							<a href="#">Contact Us</a>
							<a href="#">Popular News</a>
							<a href="#">Payment Type</a>
							<a href="#">News Type</a>
							<a href="#">Information</a>
							<a href="#">My Account</a>
							<a href="#"> Setting & Privacy</a>
						</div>
						<div class="copyright text-center">
							<p>Copyright <i class="far fa-copyright"></i> 2019, <a href="#">eKagoz</a> News Magazine PSD Template. All rights reserved. Design By <a href="#">BDevs</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>