<header>
        <div class="header-top-area blue-bg d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-9 col-lg-12 col-md-12">
                     <div class="header-wrapper">
                         <div class="header-info mr-40 f-left">
                             <h5>Headline</h5>
                         </div>
                         <div class="header-text">
                             <ul class="breaking-active owl-carousel">
                                 <li><a href="#">On the other hand we denounce with righteous indignation and dislike men who are so beguiled</a></li>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <div class="col-xl-3 col-lg-3 col-md-3 d-flex align-items-center">
                    <div class="header-top-right f-right  d-none d-xl-block">
                        <div class="header-icon f-right ">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="twitt" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="instagra" href="#"><i class="fab fa-instagram"></i></a>
                            <a class="youtub" href="#"><i class="fab fa-youtube-square"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle-area pt-10 pb-10">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 d-flex align-items-center">
                    <div class="logo">
                        <a href="index.php"><img src="assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9">
                    <div class="header-banner text-right d-none d-lg-block">
                        <a href="index.php"><img src="assets/img/banner/banner-01.jpg" alt="" /></a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="sticky-header" class="main-menu-area d-none d-lg-block">
        <div class="container">
            <div class="menu-bg grey-bg position-relative">
                <div class="row">
                    <div class="col-lg-10 position-static">
                        <div class="menu-bar f-left d-none d-lg-block">
                            <a class="info-bar" href="javascript:void(0);"><i class="fas fa-bars"></i></a>
                        </div>
                        <div class="main-menu f-left">
                            <nav id="mobile-menu">
                                <ul>
                                    <li class="active"><a href="index.php">home</a>
                                    </li>
                                    <li class="static"><a href="lifestyle.html">Lifestyle</a>
                                        <ul class="mega-menu" style="background-image: url(assets/img/bg/menu.png)">
                                            <li class="mega-item">
                                                <a>Category 1</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Technology</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">magazine</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-item">
                                                <a>Category 2</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">magazine</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Technology</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li class="mega-item">
                                                <a>Category 3</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">magazine</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Technology</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="mega-item">
                                                <a>Category 4</a>
                                                <ul>
                                                    <li>
                                                        <a href="#">magazine</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Technology</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Sports</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Lifestyle</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Pages</a>
                                        <ul class="sub-menu text-left">
                                            <li><a href="about.php">about</a></li>
                                            <li><a href="team.php">team</a></li>
                                            <li><a href="post-layout.php">Single Post</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Account</a>
                                        <ul class="sub-menu text-left">
                                            <li><a href="profile.php">Profile</a></li>
                                            <li><a href="javascript:void();">Update Profile</a></li>
                                            <li><a href="javascript:void();">Billing </a></li>
                                            <li><a href="javascript:void();">Update Membership</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="register.php">Register</a></li>
                                    <li><a href="login.php">Login</a></li>
                                    <li><a href="submit-news.php">Publish Press Release</a></li>
                                    <li><a href="video-post.php">Upload Video</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="search-icon f-left d-none d-lg-block">
                            <a href="#" data-toggle="modal" data-target="#search-modal"><i class="fas fa-search"></i></a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="extra-info extra-info-left">
        <div class="close-icon">
            <button>
                <i class="far fa-window-close"></i>
            </button>
        </div>
        <div class="logo-side mb-30">
            <a href="index-2.html">
                <img src="assets/img/logo/white.png" alt="" />
            </a>
        </div>
        <div class="side-info mb-30">
            <div class="contact-list mb-30">
                <h4>Office Address</h4>
                <p>123/A, Miranda City Likaoli
                Prikano, Dope</p>
            </div>
            <div class="contact-list mb-30">
                <h4>Phone Number</h4>
                <p>+0989 7876 9865 9</p>
                <p>+(090) 8765 86543 85</p>
            </div>
            <div class="contact-list mb-30">
                <h4>Email Address</h4>
                <p><a href="http://bdevs.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="472e29212807223f262a372b226924282a">[email&#160;protected]</a></p>
                <p><a href="http://bdevs.net/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="eb8e938a869b878ec5868a8287ab839e86c5888486">[email&#160;protected]</a></p>
            </div>
        </div>
        <div class="instagram">
            <a href="#">
                <img src="assets/img/portfolio/p1.jpg" alt="">
            </a>
            <a href="#">
                <img src="assets/img/portfolio/p2.jpg" alt="">
            </a>
            <a href="#">
                <img src="assets/img/portfolio/p3.jpg" alt="">
            </a>
            <a href="#">
                <img src="assets/img/portfolio/p4.jpg" alt="">
            </a>
            <a href="#">
                <img src="assets/img/portfolio/p5.jpg" alt="">
            </a>
            <a href="#">
                <img src="assets/img/portfolio/p6.jpg" alt="">
            </a>
        </div>
        <div class="social-icon-right mt-20">
            <a href="#">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="#">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#">
                <i class="fab fa-google-plus-g"></i>
            </a>
            <a href="#">
                <i class="fab fa-instagram"></i>
            </a>
        </div>
    </div>
</header>
<!-- header-start -->