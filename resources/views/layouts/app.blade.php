<!doctype html>
<html class="no-js" lang="zxx">
@include('includes.head')
<body>

	<!-- header-start -->
	@include('includes.header')
	<main>
		<!-- news-area-start -->
		<div class="news-area  hero-padding pt-80 pb-50">
			<div class="container">
				<div class="row">
					<div class="col-xl-8 col-lg-8">
						@yield('content')
					</div>
					<div class="col-xl-4 col-lg-4 mb-30">
						@include('includes.sidebar')
					</div>
				</div>
			</div>
		</div>
		<!-- news-area-end -->
	</main>
	<!-- footer-area-start -->
	@include('includes.footer')
	<!-- footer-area-end -->

	<!-- Modal Search -->
	<div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form>
					<input type="text" placeholder="Search here...">
					<button>
						<i class="fa fa-search"></i>
					</button>
				</form>
			</div>
		</div>
	</div>

	<!-- JS here -->
	@include('includes.scripts')

</body>
</html>