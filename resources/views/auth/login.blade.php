@extends('layouts.app')
@section('content')
<div class="contact-us-wrapper mb-30">
	<div class="section-title mb-35">
		<h4>Login</h4>
	</div>
	<form id="contacts-us-form" class="contacts-us-form" action="{{route('login.post')}}">
		<div class="row">
			@csrf
			<div class="col-md-12">
				<input type="email" name="email" placeholder="Enter Email...." value="{{old('email')}}">
			</div>
			<div class="col-md-12">
				<input type="password" name="password" placeholder="Enter Password ....">
			</div>
			<div class="col-md-12">
				<div class="contacts-us-form-button">
					<button class="btn" type="submit">
						Login
					</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection