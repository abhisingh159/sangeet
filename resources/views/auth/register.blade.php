@extends('layouts.app')
@section('content')
	<div class="contact-us-wrapper mb-30">
		<div class="section-title mb-35">
			<h4>Registration</h4>
		</div>
		<form id="contacts-us-form" class="contacts-us-form" method="post" action="{{route('register.post')}}">
			<div class="row">
				@csrf
				<div class="col-md-6">
					<input name="first_name" type="text" placeholder="First Name.... " value="{{old('first_name')}}">
					@if($errors->has('first_name'))
						<p>{{$errors->first("first_name")}}</p>
					@endif
				</div>
				<div class="col-md-6">
					<input name="last_name" type="text" placeholder="Last Name.... " value="{{old('last_name')}}">
					@if($errors->has('last_name'))
						<p>{{$errors->first("last_name")}}</p>
					@endif
				</div>
				<div class="col-md-6">
					<input name="email" type="email" placeholder="Enter Email...." value="{{old('email')}}">
					@if($errors->has('email'))
						<p>{{$errors->first("email")}}</p>
					@endif
				</div>
				<div class="col-md-6">
					<input name="password" type="password" placeholder="Enter Password ....">
					@if($errors->has('password'))
						<p>{{$errors->first("password")}}</p>
					@endif
				</div>
				<div class="col-md-6">
					<input name="phone" type="number" placeholder="Enter Phone ...." value="{{old('phone')}}">
					@if($errors->has('phone'))
						<p>{{$errors->first("phone")}}</p>
					@endif
				</div>
				<div class="col-md-6">
					<input name="company" type="text" placeholder="Enter Company Name ...." value="{{old('company')}}">
					@if($errors->has('company'))
						<p>{{$errors->first("company")}}</p>
					@endif
				</div>
				<div class="col-md-12">
					<div class="contacts-us-form-button">
						<button class="btn" type="submit">
							Register
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection